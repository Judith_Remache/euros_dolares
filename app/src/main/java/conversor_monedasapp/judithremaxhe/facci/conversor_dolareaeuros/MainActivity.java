package conversor_monedasapp.judithremaxhe.facci.conversor_dolareaeuros;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final String LOGTAG = "LogsAndroid";
    Button Ingresar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Ingresar = (Button) findViewById(R.id.buttonIngresar);
        Ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Usuario = ((EditText) findViewById(R.id.txtUsuario)).getText().toString();
                String password = ((EditText) findViewById(R.id.txtpassword)).getText().toString();
                if (Usuario.equals("admin") && password.equals("1234"))

                {
                    Intent nuevoform =new Intent(MainActivity.this,conversor.class);
                    startActivity(nuevoform);

                }
                else {
                    Toast.makeText(getApplicationContext(), "Usuario Incorrecto", Toast.LENGTH_LONG).show();
                }
            }

        });


        Log.i(LOGTAG, "Clase: MainActivity.java \nEstudiante: Judith Remache Pilco");
    }

}
