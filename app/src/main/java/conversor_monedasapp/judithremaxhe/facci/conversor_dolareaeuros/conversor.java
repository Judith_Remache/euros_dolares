package conversor_monedasapp.judithremaxhe.facci.conversor_dolareaeuros;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class conversor extends AppCompatActivity {

    EditText et_euros,et_dolares;
    Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversor);

        et_dolares=(EditText)findViewById(R.id.editTextDOLAR);
        et_euros = (EditText)findViewById(R.id.editTextEURO);
        bt =(Button)findViewById(R.id.buttonCONVERTIR);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoEuros=et_euros.getText().toString();
                Double euros = Double.parseDouble(textoEuros);

                Double dolares = euros*1.13;
                String textoDolares = String.valueOf(dolares);
                et_dolares.setText(textoDolares);
            }
        });
    }

}
